package ru.deltasolutions.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.deltasolutions.testtask.entity.Resource;

import java.util.Optional;

@Repository
public interface ResRepository extends CrudRepository<Resource, Integer> {

    Optional<Resource> findBySn(String sn);

}
