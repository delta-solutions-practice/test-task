package ru.deltasolutions.testtask.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.deltasolutions.testtask.entity.Employee;

import java.util.Optional;

@Repository
public interface EmpRepository extends CrudRepository<Employee, Integer> {

    Optional<Employee> findByName(String name);

}
