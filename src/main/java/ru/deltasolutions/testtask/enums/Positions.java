package ru.deltasolutions.testtask.enums;

public enum Positions {
    director,
    programmer,
    manager,
    accountant
}
