package ru.deltasolutions.testtask.enums;

public enum ResType {
    laptop,
    monitor,
    pc,
    accessories
}
