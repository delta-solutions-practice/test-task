package ru.deltasolutions.testtask.service;

import ru.deltasolutions.testtask.entity.Resource;
import ru.deltasolutions.testtask.enums.ResType;

import java.util.List;

public interface ResService {
    Resource getResourceById(Integer id);
    Resource getResourceBySn(String sn);
    void saveResource(Resource res);
    void updateResource(Integer id, String sn, String specs, ResType type, String name);
    void deleteResource(Integer id);
    List<Resource> findAll();
}
