package ru.deltasolutions.testtask.service;

import ru.deltasolutions.newsapi.models.ArticlesResult;
import ru.deltasolutions.testtask.entity.Employee;
import ru.deltasolutions.testtask.enums.Positions;

import java.sql.Date;
import java.util.List;

public interface EmpService {
    Employee getEmployeeById(Integer id);
    Employee getEmployeeByName(String name);
    void saveEmployee(Employee emp);
    void saveAllFromArticles(ArticlesResult result);
    void updateEmployee(Integer id, String name, Positions position, Date empDate);
    void deleteEmployee(Integer id);
    List<Employee> findAll();
}
